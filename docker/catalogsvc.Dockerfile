FROM calvinkodian/openjdk8
MAINTAINER calvin@apicomm.com
COPY files/catalog-service-0.0.1-SNAPSHOT.jar /opt/catalog-service.jar
ENV SPRING_PROFILE="" JAVA_OPTS=""
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "-Dspring.profiles.active=${SPRING_PROFILE}", "/opt/catalog-service.jar"]
EXPOSE 8080