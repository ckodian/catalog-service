FROM alpine:edge
MAINTAINER calvin@apicomm.com
RUN apk add --no-cache openjdk8
COPY UnlimitedJCEPolicyJDK8/* /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/