FROM calvinkodian/openjdk8
MAINTAINER calvin@apicomm.com
COPY files/eureka-server-0.0.1-SNAPSHOT.jar /opt/eureka-server.jar
ENV SPRING_PROFILE="" JAVA_OPTS=""
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "-Dspring.profiles.active=${SPRING_PROFILE}", "/opt/eureka-server.jar"]
EXPOSE 8671