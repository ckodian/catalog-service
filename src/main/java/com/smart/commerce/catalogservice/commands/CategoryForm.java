package com.smart.commerce.catalogservice.commands;

import java.util.Set;

public class CategoryForm {

    private String name;
    private String description;
    private Long id;
    private Set<Long> superCategories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Long> getSuperCategories() {
        return superCategories;
    }

    public void setSuperCategories(Set<Long> superCategories) {
        this.superCategories = superCategories;
    }
}
