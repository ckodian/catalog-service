package com.smart.commerce.catalogservice.service;

import com.smart.commerce.catalogservice.commands.CategoryForm;
import com.smart.commerce.catalogservice.model.Category;
import com.smart.commerce.catalogservice.repository.CategoryRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Collection<Category> getAll(){
        Iterable<Category> cateIterable = this.categoryRepository.findAll();
        List<Category> cats = new ArrayList<>();
        cateIterable.forEach(cats::add);
        return cats;
    }

    public Category getCategoryById(Long id) {
        return this.categoryRepository.findById(id).get();
    }

    public Collection<Category> findAllCategoriesByIds(List<Long> ids) {
        Iterable<Category> cateIterable = this.categoryRepository.findAllById(ids);
        List<Category> cats = new ArrayList<>();
        cateIterable.forEach(cats::add);
        return cats;
    }

    public Category createOrUpdateCategory(CategoryForm form){
        Category category = null;
        Set<Category> superCategories = new HashSet<>();

        if(form.getId() == null || this.categoryRepository.findById(form.getId()).get() == null) {
            category = new Category(null, form.getName(), form.getDescription());
        } else {
            category = this.categoryRepository.findById(form.getId()).get();
        }

        if (!CollectionUtils.isEmpty(form.getSuperCategories())) {
            this.categoryRepository.findAllById(form.getSuperCategories()).forEach(superCategories::add);
            category.setSuperCategories(superCategories);
        }

        category.setName(form.getName());
        category.setDescription(form.getDescription());
        return this.categoryRepository.save(category);
    }
}
