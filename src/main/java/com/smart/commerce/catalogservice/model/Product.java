package com.smart.commerce.catalogservice.model;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    @Relationship(value = "CATEGORY", direction = "UNDIRECTED")
    private Set<Category> superCategories;

}
