package com.smart.commerce.catalogservice.model;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Category {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    @Relationship(value = "BELONGING_TO" )
    private Set<Category> superCategories;

    public Category() {
    }

    public Category(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Category> getSuperCategories() {
        return superCategories;
    }

    public void setSuperCategories(Set<Category> superCategories) {
        this.superCategories = superCategories;
    }
}
