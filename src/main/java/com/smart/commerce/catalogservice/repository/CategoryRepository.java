package com.smart.commerce.catalogservice.repository;

import com.smart.commerce.catalogservice.model.Category;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.Optional;

public interface CategoryRepository extends Neo4jRepository<Category, Long> {

}
