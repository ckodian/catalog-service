package com.smart.commerce.catalogservice.repository;

import com.smart.commerce.catalogservice.model.Product;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface ProductRepository extends Neo4jRepository<Product, Long> {

}
