package com.smart.commerce.catalogservice.resources;

import com.smart.commerce.catalogservice.model.Product;
import com.smart.commerce.catalogservice.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/products")
public class ProductController {

    private ProductService productService;

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable Long id){
        return this.productService.findProductById(id);
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}
