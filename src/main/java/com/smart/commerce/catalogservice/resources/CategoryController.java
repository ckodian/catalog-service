package com.smart.commerce.catalogservice.resources;

import com.smart.commerce.catalogservice.commands.CategoryForm;
import com.smart.commerce.catalogservice.model.Category;
import com.smart.commerce.catalogservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/rest/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public Collection<Category> getAll(){

        return categoryService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Category findCategoryById(@PathVariable Long id){
        return categoryService.getCategoryById(id);
    }

    @PutMapping("/add")
    public Category createCategory(@RequestBody CategoryForm form) {
        return this.categoryService.createOrUpdateCategory(form);
    }

    @PostMapping("/update/{id}")
    public Category createCategory(@PathVariable Long id, @RequestBody CategoryForm form) {
        form.setId(id);
        return this.categoryService.createOrUpdateCategory(form);
    }
}
